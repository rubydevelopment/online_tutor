class Admin::AdminController < ApplicationController
	
	before_filter :is_admin?

	private

	def is_admin?
		redirect_to :root if !current_account.admin?
	end
end