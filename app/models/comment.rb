# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  title            :string(50)       default("")
#  comment          :text
#  commentable_id   :integer
#  commentable_type :string(255)
#  account_id       :integer
#  role             :string(255)      default("comments")
#  created_at       :datetime
#  updated_at       :datetime
#

class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment
  belongs_to :commentable, :polymorphic => true
  default_scope -> { order('created_at DESC') }

  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_voteable

  # NOTE: Comments belong to a user
  belongs_to :account

  validates_length_of :comment, :minimum => 1, :maximum => 200

  def self.create_comment(namespace_object, current_account, params={})
  	comment = namespace_object.comments.create
    comment.account_id = current_account.id
    comment.title = params[:title] if params[:title]
  	comment.comment = params[:comment] if params[:comment]
  	{ result: comment.save, comment: comment }
  end
end
