comments_url = "/comments"



$(window).ready () ->
  namespace_object_class = $('.comments-content').attr "namespace_object_class"
  namespace_object_id = $('.comments-content').attr "namespace_object_id"


  tinyMCE.init({
    selector: "textarea.tinymce",
    toolbar: ["styleselect | bold italic | link image | undo redo","table | fullscreen"],
    plugins: "wordcount,paste,preview,textcolor"
    });
        

  send_ajax_request = (page)->
    $.ajax {
      url: comments_url,
      data: { 'namespace_object_class': namespace_object_class, 'namespace_object_id': namespace_object_id, 'page': page || 1 },
      success: (data) ->
        $('.comments').html data
    } 

  send_ajax_request()

  $('body').on "click", ".paginator a", (event)->
    event.preventDefault()

    page = $(this).attr("href").match(/page=([^&]+)/)[1]
    send_ajax_request(page)
