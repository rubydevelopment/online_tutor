module Admin 
	class SubItemInWelcomesController < Admin::AdminController

		ITEMS = [:title, :description, :avatar]

		active_scaffold :sub_item_in_welcome do | config |
	
			config.list.label = "Blocks in main page"	
			config.create.label = "Let's make a new block, shall we?=)"

			config.actions.exclude :search

			config.columns = ITEMS
			config.list.columns = ITEMS
			config.show.columns = ITEMS
			config.create.columns = ITEMS
		end
	end
end