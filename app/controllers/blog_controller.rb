class BlogController < ApplicationController

  # => be carefull where skip authenticate
  skip_before_filter :authenticate_account!, only: [:index, :show]

  def index
    @posts = BlogPost.paginate(page: params[:page], per_page: PER_PAGE, include: :comments)
  end

  def show
    @post = BlogPost.find_by_title(params[:id])
    #@post_comments = @post.comments.paginate(page: params[:page], per_page: PER_PAGE, include: :account)

    # => is account liked this post
    @is_account_liked_it = false
    if current_account
      @is_account_liked_it = current_account.voted_for?(@post)
    end
  end

  # => like or dislike post
  # => if request is not ajax return
  def set_vote

    # => render text if request is not ajax
    render text: "" if !request.is_xhr?

    @post = BlogPost.find_by_id(params[:id])

    @is_set_vote = false
    if ["like", "bad"].include?(params[:vote])
      
      # => check if account liked it?
      if !current_account.voted_for?(@post)
        @post.vote_by(voter: current_account, vote: 'like')
        @is_set_vote = true
      end
    end

    render :js
  end
end