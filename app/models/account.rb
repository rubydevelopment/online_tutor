# == Schema Information
#
# Table name: accounts
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  profile_id             :integer
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  role                   :string(255)
#  login                  :string(255)
#

class Account < ActiveRecord::Base

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable

  has_many :profiles

  POSSIBLE_ROLES = %{ admin student teacher parent }

  # => is used for likes
  acts_as_voter

 	def self.find_for_facebook_oauth access_token
    if account = Account.where(:provider_url => access_token.info.urls.Facebook).first
      account
    else 
    	# => find by email
      Account.create!(
      										:provider_url => access_token.info.urls.Facebook, 
      										#:username => access_token.extra.raw_info.name, 
      										#:nickname => access_token.extra.raw_info.username, 
      										:email => access_token.extra.raw_info.email, 
      										:password => Devise.friendly_token[0,20]
      								) 
    end
  end

  def admin?
    self.role == "admin"
  end

end
