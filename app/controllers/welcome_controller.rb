class WelcomeController < ApplicationController

	skip_before_filter :authenticate_account!

	def index
		# => get all blocks for main page
		@welcome_blocks = SubItemInWelcome.all
	end
end
