class CommentsController < ApplicationController

  layout false

  # => if request is not ajax return ""
  before_filter :is_ajax_request?

  before_filter :get_comment_params, only: [:create]

  # => get and check all params that needed for creation of comment
  # => current account is checked in application controller
  # => we need to check:
  # => namespace_object_class: class name of instance
  # => this instance id
  before_filter :check_params_for_create_comment, only: [:create]

  before_filter :check_params_for_index, only: [:index]

  def create
    @result = Comment::create_comment(@namespace_object, current_account, @comment_params)
    @comments = get_comments_by_params(@namespace_object)
  end

  def index 
    @comments = get_comments_by_params(@namespace_object, params[:page])
  end

  private 

  def is_ajax_request?
    render text: "" if !request.xhr?
  end

  def check_params_for_create_comment

    if !@comment_params[:namespace_object_id] || !@comment_params[:namespace_object_class]
      render text: "" and return
    end

    @namespace_object = init_namespace_object(@comment_params[:namespace_object_class], @comment_params[:namespace_object_id])
  end

  def comment_params
    params.require(:comment).permit(:namespace_object_class, :namespace_object_id, :title, :comment)
  end

  def check_params_for_index
    @namespace_object = init_namespace_object(params[:namespace_object_class], params[:namespace_object_id])
  end

  def get_comment_params
    @comment_params = comment_params
  end

  def get_comments_by_params(object, page=0)
    object.comments.paginate(page: params[:page], per_page: PER_PAGE, include: :account)
  end

  def init_namespace_object(class_name, id)
    begin
      return Object.const_get(class_name).find(id)
    rescue
      render text: "" and return
    end
  end

end
