class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.

	# => default page size for will_paginate
	PER_PAGE = 10

	protect_from_forgery with: :exception

	before_action :authenticate_account!

	def current_ability
		@current_ability ||= AccountAbility.new(current_account)
	end
end
