class Accounts::OmniauthCallbacksController < ApplicationController

	# => authorization by social networks
  def facebook
	render :text => Account.find_for_facebook_oauth(request.env["omniauth.auth"]) and return
	redirect_to :root
  end

  def vkontakte
  end
end
