class AccountAbility
  include CanCan::Ability

  def initialize(account)
    account ||= Account.new # guest user (not logged in)
    if account.admin? # Admin user
      can :manage, :all
    else # Non-admin user
      can :read, :all
    end
  end
end