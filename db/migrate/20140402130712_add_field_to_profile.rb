class AddFieldToProfile < ActiveRecord::Migration
  def change

  	# => shared
  	add_column :profiles, :first_name,   :string
  	add_column :profiles, :last_name,    :string
  	#add_column :profiles, :position,     :string
    add_column :profiles, :longitude,     :double
  	add_column :profiles, :width,     :double
    add_column :profiles, :sex,          :boolean
  	add_column :profiles, :age,          :integer
  	add_column :profiles, :description,  :text
  	add_column :profiles, :phone_number, :string
  	add_column :profiles, :skype,        :string


  	# => for teacher
  	add_column :profiles, :experience, :double
  	add_column :profiles, :price,      :double
  end
end
