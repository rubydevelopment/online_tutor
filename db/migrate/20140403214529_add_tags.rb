class AddTags < ActiveRecord::Migration
  def change

    ['Development', 'Ruby on Rails', 'Ruby', 'CSS', 'CSS3', 'HTML', 'HTML5', 'Bootstrap', 'C#', 'C', 'C++', 'JAVA', 'PASCAL', 'PHP', 'Python', 'JS', 'JavaScript', 'Jquery', 'Node.js'].each do | tag |
    	execute "INSERT INTO tags (name) VALUES ('#{tag}');"
    end 
  end
end
