class CreateSubItemInWelcomes < ActiveRecord::Migration
  def change
    create_table :sub_item_in_welcomes do |t|
    	t.text   :title
    	t.text   :description
    end
  end
end
