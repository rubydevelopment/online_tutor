# == Schema Information
#
# Table name: profiles
#
#  id           :integer          not null, primary key
#  account_id   :integer
#  created_at   :datetime
#  updated_at   :datetime
#  first_name   :string(255)
#  last_name    :string(255)
#  longitude    :float
#  width        :float
#  sex          :boolean
#  age          :integer
#  description  :text
#  phone_number :string(255)
#  skype        :string(255)
#  experience   :float
#  price        :float
#

class Student < Profile
end
