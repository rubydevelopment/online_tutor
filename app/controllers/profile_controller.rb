
class ProfileController < ApplicationController

  skip_before_action :authenticate_account!, only: [:index, :show]

  def new 
    @old_profile = current_account.profiles.last
    @profile = Profile.new
  end

  def create
    @profile = Profile.new profile_params
    
    if @profile.save
        redirect_to profile_edit(@profile[:id])
    else
      render :new
    end
  end

  def index
    @profiles = Profile.paginate(page: params[:page], per_page: PER_PAGE)
  end

  def edit
    @profile = Profile.find params[:id]
  end

  def update
    @profile = Profile.find params[:id]
    @profile.update_attributes(@profile.teacher? ? teacher_params : student_params)
  end

  def show
    @profile = Profile.find(params[:id])
  end

  private 

  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :sex, :age, :phone_number, :skype)
  end

  def teacher_params
    raise "No implemented"
  end

  def student_params
    raise "No implemented"
  end
end


#  first_name   :string(255)
#  last_name    :string(255)
#  longitude    :float
#  width        :float
#  sex          :boolean
#  age          :integer
#  description  :text
#  phone_number :string(255)
#  skype        :string(255)
#  experience   :float
#  price        :float
