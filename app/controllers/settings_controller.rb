class SettingsController < ApplicationController

	# => only teacher and student can set profile
	before_filter :has_permission_to_set_profile?, only: [:step_two]

	# => step_one is impemented in view

  # => first step in registration: TODO maybe better user can change it after
  def set_role
  	role = params[:role]
  	if Account::POSSIBLE_ROLES.include? role
  		current_account.role = role
  		current_account.save!
  		redirect_to :step_two
  	else
  		flash[:alert] = "There is no role: #{role} in list"
  		render :step_one
  	end
  end

  # => set account profile and depends of role 
  def step_two
  end

  def save_profile
  end

  protected

  # => if user is not teacher or student redirect to main page
  def has_permission_to_set_profile?
    redirect_to :root if !(%{ teacher student }.include? current_account.role)
  end


end
