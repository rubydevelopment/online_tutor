module Admin
	class BlogPostsController < AdminController

		ITEMS = [:title, :description, :avatar]

		active_scaffold :blog_post do | config |
	
			config.list.label = "Posts"	
			config.create.label = "Let's make a new post, shall we?=)"

			config.actions.exclude :search

			config.columns = ITEMS
			config.list.columns = ITEMS
			config.show.columns = ITEMS
			config.create.columns = ITEMS
		end

	end
end