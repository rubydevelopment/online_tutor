# == Schema Information
#
# Table name: blog_posts
#
#  id                  :integer          not null, primary key
#  title               :string(255)
#  description         :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#

class BlogPost < ActiveRecord::Base

  #extend FriendlyId
  #friendly_id :title, use: :slugged

  validates :title, uniqueness: true
  has_attached_file :avatar, 
          styles: { medium: "300x300>", thumb: "100x100>" }, 
          default_url: "/images/:style/missing.png"

  # => VALIDATE PAPERCLIP IMAGE
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates_attachment :avatar, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  validates :avatar, attachment_presence: true
  validates_with AttachmentPresenceValidator, attributes: :avatar
  validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 3.megabytes

  validates_length_of :title, minimum: 10, maximum: 200, allow_blank: false
  validates_length_of :description, minimum: 10, maximum: 1000, allow_blank: false
  
  default_scope { order('created_at DESC') }

  acts_as_commentable
  acts_as_votable

end
