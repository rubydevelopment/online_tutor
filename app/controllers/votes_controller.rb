class VotesController < ApplicationController

  layout false

  before_filter :get_params_for_create_vote, only: [:create]

  # => if request is not ajax return ""
  before_filter :is_ajax_request?

  def create
    if ["like", "bad"].include?(@vote_params[:vote])
      
      # => check if account liked it?
      if !current_account.voted_for?(@namespace_object)
        @namespace_object.vote_by(voter: current_account, vote: @vote_params[:vote])
      end
    end
  end

  def get_params_for_create_vote

  	@vote_params = vote_params

  	if !@vote_params[:namespace_object_id] || !@vote_params[:namespace_object_class]
      render text: "" and return
    end

    @namespace_object = init_namespace_object(@vote_params[:namespace_object_class], @vote_params[:namespace_object_id])
  end

  def vote_params
    params.require(:vote).permit(:namespace_object_class, :namespace_object_id, :vote)
  end

  private 

  def is_ajax_request?
    render text: "" if !request.xhr?
  end

  def init_namespace_object(class_name, id)
    begin
      return Object.const_get(class_name).find(id)
    rescue
      render text: "" and return
    end
  end
end
