# == Schema Information
#
# Table name: profiles
#
#  id           :integer          not null, primary key
#  account_id   :integer
#  created_at   :datetime
#  updated_at   :datetime
#  first_name   :string(255)
#  last_name    :string(255)
#  longitude    :float
#  width        :float
#  sex          :boolean
#  age          :integer
#  description  :text
#  phone_number :string(255)
#  skype        :string(255)
#  experience   :float
#  price        :float
#

class Profile < ActiveRecord::Base

  belongs_to :account
  has_and_belongs_to_many :tags

  validates_length_of :first_name, minimum: 1, maximum: 200
  validates_length_of :last_name, minimum: 1, maximum: 200
  validates_length_of :phone_number, minimum: 1, maximum: 50
  validates_length_of :skype, minimum: 1, maximum: 50
  # => to do add validation for other fields

end
