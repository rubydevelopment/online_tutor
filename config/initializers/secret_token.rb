# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
OnlineTutor::Application.config.secret_key_base = 'd9a1e0dbc0574257fc69bdaa18a87ab40bcee0e72dd4b934be3f8ddf654d3a29e6822b3026bb1b3f3aa334231b420dafc55148f9fefa6c96e5afc4ce611dbd4d'
