class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|

      t.string :description
      t.integer :account_id
      t.string :permission      
      t.timestamps
    end
    add_attachment :attachments, :file
  end
end
