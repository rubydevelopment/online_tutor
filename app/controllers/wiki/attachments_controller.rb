module Wiki
  class AttachmentsController < WikiController

    ITEMS = [:description, :file, :permission]

    active_scaffold :attachment do | config |
  
      config.list.label = "Attachments" 
      config.create.label = "Let's we upload new file, shall we?=)"

      config.columns = ITEMS
      config.list.columns = ITEMS
      config.show.columns = ITEMS
      config.create.columns = ITEMS
    end
    
  end
end