class RegistrationsController < Devise::RegistrationsController

	before_filter :clear_flash

	def create	
		if verify_recaptcha(private_key: YAML.load_file('config/recaptcha.yml')['private_key']) and params[:agree_terms]
			super
		else
			build_resource
      clean_up_passwords(resource)
      flash[:alert] = "There was an error with the recaptcha code below. Please re-enter the code and click submit."

      if !params[:agree_terms]
				flash[:alert] = "You must agree with Terms of Service"      	
      end
      
      render :new
		end
	end

	def step_one
	end

	def step_two
	end

	protected

	def clear_flash
		flash[:alert] = nil
	end

end
