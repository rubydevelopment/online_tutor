OnlineTutor::Application.routes.draw do

  devise_for :accounts, :controllers => { registrations: "registrations", omniauth_callbacks: "accounts/omniauth_callbacks" }

  resources :blog, only: [:index, :show]
  
  resources :comments, only: [:create, :index]

  resources :votes, only: [:create]



  # => for registrations
  get "step1" => "settings#step_one", as: :step_one
  post "set_role" => "settings#set_role", as: :set_role
  get "step2" => "settings#step_two", as: :step_two
  post "save_profile" => "settings#save_profile", as: :save_profile

  namespace :wiki do 
    resources(:attachments) { as_routes }
  end

  root to: "welcome#index"

  namespace :admin do

    resources(:sub_item_in_welcomes) { as_routes }
    resources(:blog_posts) { as_routes }

  end



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
