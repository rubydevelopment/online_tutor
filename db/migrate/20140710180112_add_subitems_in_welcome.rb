class AddSubitemsInWelcome < ActiveRecord::Migration
  def self.up
  	add_attachment :sub_item_in_welcomes, :avatar
  end

  def self.down
  	SubItemInWelcome.delete_all
  	remove_attachment :sub_item_in_welcomes, :avatar
  end
end
